# Croix-Rouge changelog

La dernière version de ce fichier peut être trouvée sur la branche [dev](https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge/-/tree/dev) du dépôt Dev Croix-Rouge

## 2.1 (30/05/2020)

### Fixé (2 changement)
 - L'affichage du nombre de VL dans la page de détail de la demande est maintenant correct
 - Fix des permissions d'assignations dans l'état provisionné

### Modifié (5 changements)
 - Refactor des tests pour être plus cohérent
 - Assignation des véhicules sous forme de sélection véhicule par véhicule
 - Modifications associées de la page de demande
 - Ajout de Pylint à la CI
 - Refactor de la méthode est_dispo
 
### Ajouté (4 changements)
 - Nouveau menu et pages d'import pour les véhicules et traitement d'un fichier Excel; Gestion d'un type de ressource à la fois + Wiki associé
 - Assignation avancée des véhicules : sélection parmi une liste des véhicules disponibles
 - Ajout de Renovate au projet
 - Ajout de Black comme linter



## 2.0 (12/05/2020)

### Supprimé (1 changement)
 - Le champ description succinte d'une demande de mission

### Fixé (1 changement)
 - L'édition de date de mission n'est plus possible après la validation

### Modifié (7 changements)
 - Refactor complet du projet, avec mise sous forme de module des modèles, des vues, des forms
 - Assignation des véhicules et des effectifs via une nouvelle interface au lieu de champs texte,
 avec un tableau résumant les disponibilités et les affectations en temps réel
 - La page d'admin a été refaite, avec une interface de saisie et de visualisation plus simple
 - Les champs de la page de détail de demande ne coupent plus le texte trop long
 - Les saut de ligne entrés dans les formulaires sont conservés et affichés
 - Mise en forme du formulaire de demande sous forme d'onglets
 - Refonte de la CI/CD (ajout d'un Linter, et des outils gitlab SATR, Code Quality)
 
### Ajouté (9 changements)
 - Modèles complets de Ressource, RessourcePointer, Logistique, Matériel, Véhicule, Reservation, 
Effectifs et dérivés, Lieustockage, Disponibilité, Compétence
 - Modification du modèle de demande pour ajouter les champs spécifiques à des types de mission
 - Ajout de test unitaires et d'intégration, et le la CI pour les intégrer
 - Classes de retour spécifique, avec formulaires
 - Les heures de présence de bénévoles sont mises à la même valeur que les horaires de la mission dans le formulaire de demande
 - Il est possible d'activer/desactiver l'assignation "interactive" des véhicules et des bénévoles (par défaut desactivé)
 - Ce changelog  ¯\_(ツ)_/¯
 - Système d'archivage des demandes, via la page des demandes ou l'interface d'administration
 - Ajout d'une page 403

 
### Autre (0 changements)

## 1.1 (02/05/2020)

### Supprimé (0 changements)

### Fixé (1 changements)
 - Résolution d'un problème de permission de création de demande

### Modifié (2 changements)
 - Page d'erreur 403 au lieu d'une page vide pour le personnel non autorisé
 - Liste des demandes avec un tableau plus complet : ajout du champ date et de la traduction française du tableau

### Ajouté (2 changements)
 - Système basique de retour, avec les champs généraux communs à toutes les missions
 - Ajout du numéro de version sur la page d'accueil

### Autre (0 changements)


## 1.0 (04/2020)

### Supprimé (0 changements)

### Fixé (0 changements)

### Modifié (0 changements)

### Ajouté (7 changements)

 - Mise en place du projet Django
 - Système d'authentification, de groupes et de permissions
 - Modèles de Demande, de Réponse, de Profil
 - Formulaires de demande et assignations textuelles simples
 - Page de détail d'une demande
 - Page d'édition d'une demande
 - Page de liste des demandes

### Autre (0 changements)

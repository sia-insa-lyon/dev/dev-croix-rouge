"""dev_croix_rouge URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from CoronApp.views import *

urlpatterns = [
    # Administration
    path("admin/", admin.site.urls),
    path("accounts/", include("django.contrib.auth.urls")),
    # Demandes
    path("demandes/", ListDemandes.as_view(), name="demandes"),
    path("demande/<pk>", DemandeView.as_view(), name="demande"),
    path("demande/<pk>/edit", DemandeEditView.as_view(), name="demande-edit"),
    path("newdemande/", DemandeFormView.as_view(), name="demande-form"),
    # Assignations
    path("demande/<pk>/assign/<t>", assign_view, name="assign"),
    path(
        "demande/<pk>/affecter_ressource", affecter_ressource, name="affecter-ressource"
    ),
    path(
        "demande/<pk>/desaffecter_ressource",
        desaffecter_ressource,
        name="desaffecter-ressource",
    ),
    path("demande/<pk>/assign_ressource", set_assign, name="assign-ressource"),
    path("demande/<pk>/finir_assignation", finir_assignation, name="finir-assignation"),
    path(
        "demande/<pk>/enregistrer_commentaire_assignation",
        enregistrer_commentaire_assignation,
        name="enregistrer_commentaire_assignation",
    ),
    # Opérations sur les demandes
    path("demande/<pk>/cloture", cloture, name="cloture"),
    path("demande/<pk>/validate", validate, name="validate"),
    path("demande/<pk>/archive", archive, name="archive"),
    path("demande/<pk>/refuse", refuse, name="refuse"),
    path("demande/<pk>/retour", RetourView.as_view(), name="retour-form"),
    # Impports
    path("imports", imports_view, name="imports"),
    # Page d'acceuil
    path("", index),
]

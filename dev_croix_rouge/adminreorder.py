ADMIN_REORDER = (
    {
        "app": "CoronApp",
        "label": "Utilisateur",
        "models": ("CoronApp.Profil", "auth.Group", "auth.Permission", "auth.User",),
    },
    {
        "app": "CoronApp",
        "label": "Demande",
        "models": (
            "CoronApp.Demande",
            "CoronApp.Reponse",
            "CoronApp.Retour",
            "CoronApp.TypeMission",
            "CoronApp.TypeInfo",
            "CoronApp.Infos",
        ),
    },
    {
        "app": "CoronApp",
        "label": "Effectif",
        "models": (
            "CoronApp.Effectif1J",
            "CoronApp.EffectifCRF",
            "CoronApp.Competence",
            "CoronApp.TypeCompetence",
        ),
    },
    {
        "app": "CoronApp",
        "label": "Véhicule",
        "models": (
            "CoronApp.Vehicule",
            "CoronApp.TypeVehicule",
            "CoronApp.CategorieVehicule",
        ),
    },
    {
        "app": "CoronApp",
        "label": "Matériel",
        "models": (
            "CoronApp.Materiel",
            "CoronApp.TypeMateriel",
            "CoronApp.CategorieMateriel",
        ),
    },
    {
        "app": "CoronApp",
        "label": "Logistique",
        "models": (
            "CoronApp.Reservation",
            "CoronApp.Disponibilite",
            "CoronApp.LieuStockage",
        ),
    },
)

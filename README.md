# Projet Croix rouge


[![pipeline status](https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge/badges/dev/pipeline.svg)](https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge/-/commits/dev)
[![coverage report](https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge/badges/dev/coverage.svg)](https://gitlab.com/sia-insa-lyon/dev/dev-croix-rouge/-/commits/dev)

## Sommaire 
- [Guide d'utilisation](#guide-dutilisation)
  - [Etapes de création d'une demande](#Etapes-de-cr%C3%A9ation-dune-demande)
- [Pour les administrateurs](#pour-les-administrateurs)
  - [Connection à la page d'administration](#Connection%20%C3%A0%20la%20page%20d%27administration)
  - [Création d'utilisateur](#Cr%C3%A9ation%20d%27utilisateur)
  - [Création du profil](#Cr%C3%A9ation-du-profil)
- [Pour les développeurs](#ouvrir-le-projet-dans-un-container-docker)  
- [Plus d'informations](#plus-dinformations)

## Guide d'utilisation

### Etapes de création d'une demande

1. Créer une demande via le formulaire sur la page "Nouvelle demande". La demande apparaît comme créée dans la page de gestion des demandes.
2. Le personnel autorisé valide ou refuse la demande dans la page de détail accessible via la page de gestion des demandes. La demande apparaît alors comme validée ou comme refusée.
3. Si la demande est acceptée, chaque personnel gérant les effectifs, les véhicules, la logistique ou les subsistances peut provisionner la demande en cliquant sur "Assigner ...". Pour plus d'information sur l'assignation de ressources, se réferer au paragraphe *Provisionner une demande: l'exemple des véhicules* 
4. Lorsque ces 4 domaines sont provisionnés, ie ils apparaissent tous cochés dans la page de détail. L'état est alors changé à "provisionné".
5. La demande provisionnée, un personnel autorisé peut clôturer la demande en pressant le bouton éponyme. L'état est alors "cloturé"
Le matériel provisionné est alors verouillé et non modifiable, de même que la demande initiale pour n'importe qui.
6. Une fois la mission terminée, le responsable de la mission fait un retour via la page de détail, un formulaire apparaissant dès la clôture. Le retour validé, la demande apparait dans son état final, 'terminée".

#### Provisionner une demande: l'exemple des véhicules

![image](uploads/f58e742d5e1dd0d27156aeddfef2aa8d/image.png)

Après avoir cliqué sur *Assigner véhicules*, vous êtes dirigé vers la page suivante (ou similaire).
Vous avez alors une liste de véhicules (cette liste est limitée par défaut à 10 véhicules). 
Vous pouvez filtrer ces derniers par type en cliquant sur les pastilles grises. 
Si vous savez directement la dénomination du véhicule que vous souhaitez utiliser pour la mission, vous pouvez le rechercher à l'aide de la barre de recherche.

La disponibilité d'un véhicule est indiquée sur la première colonne. 
Si un véhicule est disponible, un bouton cerclé de vert *Affecter* apparaît. 
Un clic sur ce dernier assignera le véhicule à la mission. 
Ce véhicule devient ainsi indisponible sur les créneaux horaires définis dans la demande de mission. 
Ainsi, si j'affecte le VL567 sur une mission de 14h à 16h, je ne pourrais pas l'affecter sur une mission de 15h à 16h le même jour.
Je pourrais néanmoins l'assigner sur une mission de 16h à 18h. 

Dès que le bouton *Affecter* est cliqué, le véhicule devient instantanément indisponible (il peut paraitre encore disponible pour un autre utilisateur qui n'a pas actualisé sa page, mais il sera impossible pour lui de réserver la ressource).
Pour liberer la ressource, il faut cliquer sur le bouton *Désaffecter* (Les autres utilisateurs ne pourront voir la ressource comme disponible qu'une fois la page actualisée).

Cliquer sur le bouton *Finir l'assignation* déclare l'assignation comme terminée. 
Cela permet d'indiquer à chacun que la demande peut être cloturée.  


## Pour les administrateurs

### Connection à la page d'administration

Pour se connecter à la page d'administration, il faut se rendre sur l'url `https://croixrouge.asso-insa-lyon.fr/admin/`.
Là, il faudra se connecter avec des identifiants d'un administrateur (utilisateur avec le statut équipe)

#### Création d'utilisateur

Pour ajouter un membre du staff (gestionnaire, admin, ...), il faut se rendre sur la page d'administration et cliquer sur `Utilisateurs`
Ensuite:

1. Cliquer sur nouvel utilisateur
2. Rentrer un login (par exemple hreymond pour hugo reymond)
3. Entrez un mot de passe provisoire
4. Remplir les infos personnelles (nom, prénom, adresse mail)
5. Ajouter le groupe de l'utilisateur
6. Enregistrer
7. Envoyer le lien suivant à l'utilisateur pour qu'il se recrée un mot de passe `https://croixrouge.asso-insa-lyon.fr/accounts/password_change/`

### Création du profil

Pour qu'un utilisateur soit disponible dans la liste des responsables, il faut lui créer un profil. 
1. Retourner sur la page d'administration
2. Cliquer sur Profils->Ajouter Profil
3. Chercher l'utilisateur et lui affecter un mot de passe  


[Pour les développeurs:]
## Mettre en place l'environnement de développement
Dépendances nécessaires : `git`, `python3.7+`, `pip`, `virtualenv`

Pour lancer le projet:
- Cloner avec pycharm (ou ce que vous voulez d'autre en soi): `git clone https://gitlab.com/sia-insa-lyon/dev-croix-rouge`
- Se déplacer dans le dépot cloné:  `cd dev-croix-rouge`
- (Conseillé) Mettre en place un environnement local python : `virtualenv venv && source venv/bin/activate`
- Installer les requirements `pip install -r requirements.txt`
- Appliquer les migrations de la base de donnée `python manage.py migrate`
- Lancer le serveur (Bouton play django)  ou `python manage.py runserver`
- Il faut aussi penser à mettre la variable d'environnement DEBUG à True dans `settings.py` (je le fais dans les options de lancement de pycharm perso) pour avoir les messages d'erreurs et éviter l'erreur 400 au démarrage
- Pour se créer un utilisateur admin, utilisez la commande `python manage.py createsuperuser`


Le code est passé dans le linter [Black](https://github.com/psf/black) à chaque commit. Si le code n'est pas formaté
comme il faut, la pipeline échoue. Il faut donc passer le code au linter avant de l'envoyer. Pour cela, importez Black
via `pip` avec la commande `pip install black` puis `black CoronApp/ dev_croix_rouge/` pour reformater le code.

Détail des modules des requirements hors déploiement :
- `django-fsm`: permet une gestion des états dans les modèles Django, utilisé pour les états d'une demande (fsm=final state machine)
- `django-cripsy-forms`: de jolis formulaires customisables, cf `forms.py`

## Tests

Les tests sont disponibles dans le dossier tests.
`python manage.py test CoronApp/tests` permet de les lancer.

Ils sont lancés automatiquements à chaque push. 


## Plus d'informations

Pour plus d'informations, visitez le [wiki](https://gitlab.com/sia-insa-lyon/dev-croix-rouge/-/wikis/home)

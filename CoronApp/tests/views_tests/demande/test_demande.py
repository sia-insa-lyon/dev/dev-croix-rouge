from django.core.exceptions import PermissionDenied
from django.test import TestCase, RequestFactory, Client
from CoronApp.views.demande import demande
from django.contrib.contenttypes.models import ContentType
from CoronApp.models import Demande, RetourNonDefini, RetourDesserrement
from datetime import datetime, timezone
from django.urls import reverse
from django.contrib.auth.models import AnonymousUser, User
from django.contrib.auth.models import Permission

from pprint import pprint


class TestDemande(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username="jacquot", email="jacques@pierre.com", password="password"
        )
        self.user2 = User.objects.create_user(
            username="pierre", email="pierre@jacques.com", password="password"
        )
        self.user.save()
        self.user2.save()
        self.request_factory = RequestFactory()

    def createDemands(self):
        # Demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.save()
        self.demande.validate()
        self.demande.reponse.provisionneEffectif()
        self.demande.reponse.provisionneVehicule()
        self.demande.reponse.provisionneLogistique()
        self.demande.reponse.provisionneSubsistance()
        self.demande.cloture()
        self.demande.created_by = self.user
        self.demande.save()

        # Demande avec retour specifique
        self.demande2 = Demande(titre="Croix Rouge chez vous")
        self.demande2.type = "desserrement"
        self.demande2.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande2.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande2.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande2.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande2.save()
        self.demande2.validate()
        self.demande2.reponse.provisionneEffectif()
        self.demande2.reponse.provisionneVehicule()
        self.demande2.reponse.provisionneLogistique()
        self.demande2.reponse.provisionneSubsistance()
        self.demande2.cloture()
        self.demande2.created_by = self.user2
        self.demande2.save()

    def login(self):
        self.client.login(username="jacquot", password="password")

    # ListDemandes
    def test_access_demande(self):
        self.login()
        response = self.client.get("/demandes/", follow=True)
        self.assertEqual(response.status_code, 200)

    # DemandeFormView
    def test_form_anonymous_user(self):
        res = self.client.get(reverse("demande-form"), follow=True)
        self.assertTrue(len(res.redirect_chain) == 2)
        redirect_url = res.redirect_chain[0][0]
        # Redirect to login view
        self.assertEqual(redirect_url.split("?")[0], "/accounts/login")

    def test_form_no_permission(self):
        self.login()
        response = self.client.get(reverse("demande-form"))
        self.assertEqual(response.status_code, 403)

    def test_form_has_permission(self):
        permission = Permission.objects.get(codename="add_demande")
        self.user.user_permissions.add(permission)
        self.user.save()
        self.login()
        res = self.client.get(reverse("demande-form"))
        self.assertEqual(res.status_code, 200)

    # DemandeView
    def test_demande_retour(self):
        self.createDemands()

        # Not logged in
        res = self.client.get(reverse("demande", kwargs={"pk": 1}), follow=True)
        self.assertTrue(len(res.redirect_chain) == 2)
        redirect_url = res.redirect_chain[0][0]
        # Redirect to login view
        self.assertEqual(redirect_url.split("?")[0], "/accounts/login")

        # Object not found
        self.login()
        res = self.client.get(reverse("demande", kwargs={"pk": 4}))
        self.assertEqual(res.status_code, 404)

        res = self.client.get(reverse("demande", kwargs={"pk": self.demande.id}))  # 1
        self.assertEqual(res.status_code, 200)
        self.assertIsNotNone(res.context["demande"])
        # without retour spécifique
        self.assertIsInstance(res.context["retour_spe"], RetourNonDefini)

        res = self.client.get(reverse("demande", kwargs={"pk": self.demande2.id}))  # 2
        self.assertEqual(res.status_code, 200)
        # with retour spécifique
        self.assertIsNotNone(res.context["demande"])
        self.assertIsInstance(res.context["retour_spe"], RetourDesserrement)

    # DemandeEditView
    def test_get_edit_demande_redirect(self):
        res = self.client.get(reverse("demande-edit", kwargs={"pk": 10}), follow=True)
        self.assertTrue(len(res.redirect_chain) == 2)
        redirect_url = res.redirect_chain[0][0]
        # Redirect to login view
        self.assertEqual(redirect_url.split("?")[0], "/accounts/login")

    def test_get_edit_demande_404(self):
        self.login()
        # Editing a not existing demande
        res = self.client.get(reverse("demande-edit", kwargs={"pk": 10}))
        self.assertEqual(res.status_code, 404)

    def test_get_edit_demande_id_etat_cree(self):
        self.login()
        self.createDemands()

        req = self.request_factory.get(reverse("demande-edit", kwargs={"pk": 1}))
        req.user = self.user

        # Etat de la demande différent de 'cree'
        with self.assertRaises(PermissionDenied):
            res = demande.DemandeEditView.as_view()(req, pk=1)

        self.demande.etat = "cree"
        self.demande.save()
        self.demande2.etat = "cree"
        self.demande2.save()
        # Etat cree et Demande appartient au user `self.user`
        res = demande.DemandeEditView.as_view()(req, pk=1)
        self.assertEqual(res.status_code, 200)

        # Demande n'appartient pas au user `self.user`
        with self.assertRaises(PermissionDenied):
            res = demande.DemandeEditView.as_view()(req, pk=2)

    def test_get_edit_demande_permission(self):
        self.login()
        self.createDemands()

        # Pas de permission pour l'édition, état à "cloture"
        response = self.client.get(
            reverse("demande-edit", kwargs={"pk": self.demande2.id})
        )
        self.assertEqual(response.status_code, 403)

        self.demande2.etat = "cree"
        self.demande2.save()
        content_type = ContentType.objects.get_for_model(Demande)
        permission = Permission.objects.get(
            codename="can_edit", content_type=content_type
        )

        self.user.user_permissions.add(permission)
        self.user.save()

        # Permission d'édition et état de la demande différent de 'cloture' | 'termine'
        res = self.client.get(reverse("demande-edit", kwargs={"pk": self.demande2.id}))
        self.assertEqual(res.status_code, 200)

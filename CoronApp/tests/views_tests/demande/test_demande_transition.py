from datetime import datetime, timezone

from django.contrib.auth.models import User, Permission
from django.test import Client, TestCase
from django.urls import reverse
from django_fsm import TransitionNotAllowed

from CoronApp.models import Profil, Demande


class TestDemandeTransition(TestCase):
    def setUp(self):
        self.client = Client()

        # Random user
        self.random_user = User(username="user", password="mdp")
        self.random_user.save()

        self.random_profil = Profil(user=self.random_user, tel="067557756")
        self.random_profil.save()

        # Administrateur
        self.chef = User(username="chef", password="mdp")
        self.chef.save()
        self.chef.user_permissions.add(Permission.objects.get(codename="can_validate"))
        self.chef.user_permissions.add(Permission.objects.get(codename="can_cloture"))
        self.chef.user_permissions.add(Permission.objects.get(codename="can_archive"))
        self.chef.user_permissions.add()
        self.chef.save()

        self.responsable_profil = Profil(user=self.chef, tel="067557756")
        self.responsable_profil.save()

        # Demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.responsable = self.random_profil
        self.demande.save()

        # Demande 2
        self.demande2 = Demande(titre="Croix Rouge chez vous")
        self.demande2.type = "desserrement"
        self.demande2.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande2.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande2.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande2.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande2.save()

    def assert_redirects_to_login_page(self, response):
        self.assertEqual(response.status_code, 302)
        self.assertTrue(len(response.redirect_chain) == 2)
        redirect_url = response.redirect_chain[0][0]
        # Redirect to login view
        self.assertEqual(redirect_url.split("?")[0], "/accounts/login")

    def provisionner_demande(self, demande):
        demande.reponse.provisionneEffectif()
        demande.reponse.provisionneVehicule()
        demande.reponse.provisionneLogistique()
        demande.reponse.provisionneSubsistance()
        demande.save()

    def test_not_logged(self):
        response = self.client.get("/demande/" + str(self.demande.id) + "/validate")
        self.assertEquals(response.status_code, 302)

        response = self.client.get("/demande/" + str(self.demande.id) + "/cloture")
        self.assertEquals(response.status_code, 302)

        response = self.client.get("/demande/" + str(self.demande.id) + "/refuse")
        self.assertEquals(response.status_code, 302)

    def test_logged_no_permissions(self):
        self.client.force_login(self.random_user)

        response = self.client.get("/demande/" + str(self.demande.id) + "/validate")
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(Demande.objects.get(id=self.demande.id).etat, "cree")

        response = self.client.get("/demande/" + str(self.demande.id) + "/cloture")
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(Demande.objects.get(id=self.demande.id).etat, "cree")

        response = self.client.get("/demande/" + str(self.demande.id) + "/refuse")
        self.assertNotEqual(response.status_code, 200)
        self.assertEqual(Demande.objects.get(id=self.demande.id).etat, "cree")

    def test_logged_bad_transitions(self):
        self.client.force_login(self.chef)
        with self.assertRaises(TransitionNotAllowed):
            response = self.client.get("/demande/" + str(self.demande.id) + "/cloture")

        with self.assertRaises(TransitionNotAllowed):
            response = self.client.get("/demande/" + str(self.demande.id) + "/archive")

    def test_transitions(self):
        self.client.force_login(self.chef)

        # Transition cree -> valide
        self.assertFalse(hasattr(self.demande, "reponse"))
        self.assertEqual(self.demande.etat, "cree")
        response = self.client.get("/demande/" + str(self.demande.id) + "/validate")
        self.assertEqual(response.status_code, 200)
        self.demande = Demande.objects.get(id=self.demande.id)
        self.assertEqual(self.demande.etat, "valide")
        self.assertTrue(hasattr(self.demande, "reponse"))

        # Transition valide -> cre: impossible
        with self.assertRaises(TransitionNotAllowed):
            response = self.client.get("/demande/" + str(self.demande.id) + "/validate")

        self.provisionner_demande(self.demande)
        self.demande = Demande.objects.get(id=self.demande.id)

        # Transition provisionne -> cloture
        self.assertEqual(self.demande.etat, "provisionne")
        response = self.client.get("/demande/" + str(self.demande.id) + "/cloture")
        self.assertEqual(response.status_code, 200)
        self.demande = Demande.objects.get(id=self.demande.id)
        self.assertEqual(self.demande.etat, "cloture")

        # Transition cloture -> termine
        self.demande.termine()
        self.demande.save()

        # Transition cloture -> archive
        self.assertEqual(self.demande.etat, "termine")
        response = self.client.get("/demande/" + str(self.demande.id) + "/archive")
        self.assertEqual(response.status_code, 200)
        self.demande = Demande.objects.get(id=self.demande.id)
        self.assertEqual(self.demande.etat, "archive")

        # Transition cree -> refuse
        self.assertFalse(hasattr(self.demande2, "reponse"))
        self.assertEqual(self.demande2.etat, "cree")
        response = self.client.get("/demande/" + str(self.demande2.id) + "/refuse")
        self.assertEqual(response.status_code, 200)
        self.demande2 = Demande.objects.get(id=self.demande2.id)
        self.assertEqual(self.demande2.etat, "refuse")
        self.assertFalse(hasattr(self.demande2, "reponse"))

        # Transition refuse -> all: impossible
        with self.assertRaises(TransitionNotAllowed):
            response = self.client.get("/demande/" + str(self.demande.id) + "/validate")
        with self.assertRaises(TransitionNotAllowed):
            response = self.client.get("/demande/" + str(self.demande.id) + "/refuse")
        with self.assertRaises(TransitionNotAllowed):
            response = self.client.get("/demande/" + str(self.demande.id) + "/cloture")

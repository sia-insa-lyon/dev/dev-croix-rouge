from datetime import datetime, timezone
from unittest import skip, skipIf, skipUnless

from django.contrib.auth.models import User, Permission
from django.test import TestCase

from CoronApp.models import Demande, Profil

"""
Classe de test des endpoints


- Teste que l'utilisateur n'ai pas le droit d'accéder à la page si il n'est pas loggé 
- Teste que l'utilisateur loggé avec les bonnes permissions ai le droit d'accéder à la page

Teste uniquement les requêtes GET.
Il faut obligatoirement renseigner l'url (self.url). 
La permission est quand à elle optionnelle et doit être remplie uniquement si une permission est nécessaire pour accéder à la page. 
"""


class EndpointTest(TestCase):
    __test__ = False
    permission_name: str = None
    url: str = None

    # Fonction utilitaire qui ajoute une permission à l'utilisateur user et le login
    def addPermission(self, user: User, permission_name: str):
        user.save()
        user.user_permissions.add(Permission.objects.get(codename=permission_name))
        user.save()
        self.client.force_login(User.objects.get(id=user.id))

    # On teste que si l'utilisateur n'est pas loggé, il soit renvoyé vers la page de login
    def test_not_logged(self):
        if not self.__test__:
            return
        response = self.client.get(self.url, follow=True)
        # self.assertTrue(len(response.redirect_chain) == 2)
        redirect_url = response.redirect_chain[0][0]
        # Redirect to login view
        # On vérifie que l'on est bien redirigé vers la page de login. (on utilise find car des fois l'url est /accounts/login et des fois /accounts/login/)
        self.assertNotEqual(redirect_url.split("?")[0].find("/accounts/login"), -1)

    # On teste le comportement si l'utilisateur n'a pas de permission
    def test_no_permission(self):
        if not self.__test__:
            return
        # On loggue un user random
        user = User()
        user.save()
        self.client.force_login(user)
        # On accède à la page
        response = self.client.get(self.url)

        # Si une permission est requise pour accéder à la page
        if self.permission_name:
            # Si on a renseigné self.permission_name, on s'attend à ce que la page nous renvoie une 403
            self.assertEqual(response.status_code, 403)
        else:
            # Sinon, cela veut dire qu'il ne faut pas de permission pour accéder à la page, on s'attend donc à une 200
            self.assertEqual(response.status_code, 200)

    # On test si l'utilisateur a une permission
    def test_get_permission(self):
        if not self.__test__:
            return
        # Si une permission est requise pour accéder à la page
        if self.permission_name:
            # On crée un utilisateur et on lui assigne la permission permission_name
            self.addPermission(User(), self.permission_name)
            response = self.client.get(self.url)
            self.assertEqual(response.status_code, 200)


class AssignationTest(EndpointTest):
    # __test__ détermine si le test est lancé. Par défaut à false car on ne teste pas les méthodes abstraites
    __test__ = False
    type = None

    def __init__(self, args):
        super().__init__(args)
        # Si on execute ce test case
        if self.__test__:
            self.permission_name = "can_assign_" + self.type
        # L'affectation de l'url se fait dans la fonction setUp (car elle nécessite le numéro de demande

    def setUp(self):
        if not self.__test__:
            return
        # Responsable
        self.responsable = User(username="responsable", password="mdp")
        self.responsable.save()

        self.responsable_profil = Profil(user=self.responsable, tel="067557756")
        self.responsable_profil.save()

        # Demande
        self.demande = Demande(titre="Croix Rouge chez vous")
        self.demande.date_presence_debut = datetime(
            2020, 4, 21, 10, 00, tzinfo=timezone.utc
        )
        self.demande.date_presence_fin = datetime(
            2020, 4, 21, 12, 00, tzinfo=timezone.utc
        )
        self.demande.date_debut = datetime(2020, 4, 21, 10, 00, tzinfo=timezone.utc)
        self.demande.date_fin = datetime(2020, 4, 21, 12, 00, tzinfo=timezone.utc)
        self.demande.responsable = self.responsable_profil
        self.demande.save()
        self.demande.validate()
        self.demande.save()

        self.url = "/demande/" + str(self.demande.id) + "/assign/" + self.type

    def post_permission_test(self, url: str, payload, permission_name: str):
        """
         Fonction générique pour tester un endpoint Post avec une permission

         Comprend les test suivants:
             - requête get -> 405
             - pas de permission -> Permission Error
             - permission -> ça marche
             TODO:
              - ajouter pas de payload = 400
         """
        if not self.__test__:
            return
        # On crée un utilisateur
        user = User(username="user", password="mdp")
        user.save()

        self.client.force_login(user=user)

        # Vérification Get impossible
        response = self.client.get(url, payload, content_type="application/json")
        self.assertEquals(response.status_code, 405)
        # On s'assure que l'user n'a pas le droit si il n'a pas la permission
        response = self.client.post(url, payload, content_type="application/json")
        self.assertEqual(response.status_code, 403)

        # On lui ajoute la permission
        self.addPermission(user, permission_name)
        self.client.force_login(user=user)
        # On check si il a les permissions
        response = self.client.post(url, payload, content_type="application/json")
        self.assertEqual(response.status_code, 200)
        user.delete()

    def test_post_commentaire(self):
        if not self.__test__:
            return
        url = (
            "/demande/" + str(self.demande.id) + "/enregistrer_commentaire_assignation"
        )

        self.responsable.user_permissions.all().delete()
        self.responsable.save()
        self.client.force_login(User.objects.get(id=self.responsable.id))

        # On fait un commentaire sur la ressource type, avec le commentaire "Test"
        self.post_permission_test(
            url,
            {"type": self.type, "commentaire": "Test"},
            permission_name=self.permission_name,
        )
        self.demande = Demande.objects.get(id=self.demande.id)
        # On vérifie que le commentaire est le bon
        self.assertEquals(
            getattr(self.demande.reponse, "commentaire_" + self.type), "Test"
        )
        # Et que la ressource n'est pas provisionnée
        self.assertFalse(getattr(self.demande.reponse, self.type + "_provisionnes"))

    def test_post_finir_assignation(self):
        if not self.__test__:
            return

        url = "/demande/" + str(self.demande.id) + "/finir_assignation"

        self.assertFalse(self.demande.reponse.vehicules_provisionnes)
        # On fait un commentaire sur la ressource type, avec le commentaire "Test"
        self.post_permission_test(
            url,
            {"type": self.type, "commentaire": "Test"},
            permission_name=self.permission_name,
        )
        self.demande = Demande.objects.get(id=self.demande.id)
        # On vérifie que le commentaire est le bon
        self.assertEquals(
            getattr(self.demande.reponse, "commentaire_" + self.type), "Test"
        )
        # Et que la ressource n'est pas provisionnée
        self.assertTrue(getattr(self.demande.reponse, self.type + "_provisionnes"))

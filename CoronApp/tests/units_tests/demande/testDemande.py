from django.test import TestCase
from CoronApp.models import Demande, Reponse, Retour
from datetime import datetime, timezone
from django_fsm import TransitionNotAllowed


class DemandeTestCase(TestCase):
    def setUp(self):
        self.demande = Demande(
            titre="0405 Récolte courses Auchan Dardilly",
            date_debut=datetime(2020, 5, 3, 10, 0, tzinfo=timezone.utc),
            date_fin=datetime(2020, 5, 4, 11, 0, tzinfo=timezone.utc),
            lieu_deroulement="Centre commercial Auchan - Avenue de la Porte de Lyon, 69570 Dardilly",
            description_detaillee="Récole de courses, ne pas payer sur place, règlement grâce au compte client avec bon de commande. Demander un ticket de caisse si possible. Liste jointe à votre fiche mission.",
            nb_benevoles=2,
            date_presence_debut=datetime(2020, 5, 4, 9, 0, tzinfo=timezone.utc),
            date_presence_fin=datetime(2020, 5, 4, 11, 0, tzinfo=timezone.utc),
            lieu_rdv="Mermoz",
        )
        self.demande.save()

    def provisionner(self):
        self.demande.reponse.effectifs_provisionnes = True
        self.demande.reponse.logistique_provisionnes = True
        self.demande.reponse.subsistances_provisionnes = True
        self.demande.reponse.vehicules_provisionnes = True

    def test_demande_etat_intial(self):
        self.assertEqual(self.demande.etat, "cree")

    def test_inv_transition_creation_reste(self):
        with self.assertRaises(TransitionNotAllowed):
            self.demande.provisionne()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.cloture()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.termine()

    def test_demande_refuse(self):
        self.demande.refuse()
        self.assertEqual(self.demande.etat, "refuse")

    def test_inv_transition_refuse_reste(self):
        # Ne peut pas passer dans un autre state
        self.demande.refuse()
        with self.assertRaises(TransitionNotAllowed):
            self.demande.refuse()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.provisionne()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.cloture()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.termine()

    def test_has_retour_spe(self):
        self.demande.type = "non_defini"
        self.assertFalse(self.demande.has_retour_spe())
        self.demande.type = "logistique"
        self.assertTrue(self.demande.has_retour_spe())

    def test_has_demande_spe(self):
        self.demande.type = "maraude"
        self.assertTrue(self.demande.has_demande_spe())
        self.demande.type = "logistique"
        self.assertFalse(self.demande.has_demande_spe())

    def test_demande_validate_creation_reponse(self):
        self.demande.validate()
        self.assertEqual(self.demande.etat, "valide")
        reponse = Reponse.objects.get(demande=self.demande)
        self.assertIsNotNone(reponse)

    def test_inv_transition_valide_reste(self):
        self.demande.validate()

        # Ne peut pas passer dans un autre state
        with self.assertRaises(TransitionNotAllowed):
            self.demande.refuse()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.validate()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.cloture()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.termine()

    def test_demande_pas_provisionne_exception(self):
        self.demande.validate()
        self.assertRaises(TransitionNotAllowed, self.demande.provisionne)

    def test_demande_provisionne(self):
        self.demande.validate()
        self.demande.reponse = Reponse(self.demande)
        self.provisionner()
        self.demande.save()
        self.demande.provisionne()
        self.assertEqual(self.demande.etat, "provisionne")

    def test_inv_transition_provisionne_reste(self):
        self.demande.validate()
        self.provisionner()
        self.demande.provisionne()

        # Ne peux pas passer dans un autre state
        with self.assertRaises(TransitionNotAllowed):
            self.demande.refuse()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.validate()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.provisionne()

        with self.assertRaises(TransitionNotAllowed):
            self.demande.termine()

    def test_demande_cloture_valide(self):
        self.demande.validate()
        self.demande.reponse = Reponse(self.demande)
        self.provisionner()
        self.demande.provisionne()
        self.demande.cloture()

        retour = Retour.objects.get(demande=self.demande)
        self.assertIsNotNone(retour)
        self.assertEqual(self.demande.etat, "cloture")

    def test_demande_termine_valide(self):
        self.demande.validate()
        self.demande.reponse = Reponse(self.demande)
        self.provisionner()
        self.demande.provisionne()
        self.demande.cloture()
        self.demande.termine()
        self.assertEqual(self.demande.etat, "termine")

    def test_demande_archive_valide(self):
        self.demande.validate()
        self.demande.reponse = Reponse(self.demande)
        self.provisionner()
        self.demande.provisionne()
        self.demande.cloture()
        self.demande.termine()
        self.demande.archive()
        self.assertEqual(self.demande.etat, "archive")

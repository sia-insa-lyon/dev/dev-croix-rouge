# Generated by Django 3.0.5 on 2020-05-08 15:45

from django.db import migrations, models
import django_fsm


class Migration(migrations.Migration):

    dependencies = [
        ("CoronApp", "0013_auto_20200508_1209"),
    ]

    operations = [
        migrations.AlterModelOptions(
            name="demande",
            options={
                "ordering": ["date_debut", "responsable", "titre"],
                "permissions": [
                    ("can_validate", "Can validate request"),
                    ("can_cloture", "Can cloture"),
                    ("can_edit", "Can edit a demand"),
                    ("can_refuse", "Can refuse a demand"),
                    ("can_finish", "Can add feedback"),
                    ("can_archive", "Can archive demande"),
                ],
            },
        ),
        migrations.AlterField(
            model_name="demande",
            name="etat",
            field=django_fsm.FSMField(
                choices=[
                    ("termine", "Terminée"),
                    ("archive", "Archivée"),
                    ("valide", "Validée"),
                    ("provisionne", "Provisionnée"),
                    ("cloture", "Cloturée"),
                    ("refuse", "Refusée"),
                    ("cree", "Créée"),
                ],
                default="cree",
                max_length=50,
            ),
        ),
        migrations.AlterField(
            model_name="demande",
            name="type",
            field=models.CharField(
                choices=[
                    ("distribution", "Distribution alimentaire"),
                    ("aide_alim", "Aide alimentaire"),
                    ("mission_urgence", "Mission d'urgence"),
                    ("desserrement", "Centre de desserrement"),
                    ("cr_chez_vous", "Croix-Rouge chez vous"),
                    ("accueil_ecoute", "Accueil-écoute"),
                    ("non_defini", "Non précisé"),
                    ("continuite", "Continuité des activités"),
                    ("logistique", "Logistique"),
                    ("maraude", "Maraude sanitaire"),
                    ("soutien_op", "Soutien à l'opération"),
                    ("accueil_jour", "Accueil de jour"),
                    ("secours_publics", "Renfort secours publics"),
                    ("colis_urgence", "Colis d'urgence"),
                ],
                default="non_defini",
                max_length=100,
                verbose_name="Type de mission",
            ),
        ),
    ]

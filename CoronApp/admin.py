from django.contrib import admin, messages
from django.contrib.auth.models import Permission
from django.template.defaultfilters import pluralize
from django_fsm import TransitionNotAllowed

from CoronApp.models import *

admin.site.site_header = "Administration Croix Rouge"


class OurAdmin(admin.ModelAdmin):
    def delete_queryset(self, request, queryset):
        for obj in queryset:
            obj.delete()


# Utilisateur
@admin.register(Profil)
class ProfilAdmin(OurAdmin):
    list_display = (
        "__str__",
        "tel",
    )
    search_fields = (
        "user__first_name",
        "user__last_name",
    )


# Demande
def archiver_demande(modeladmin, request, queryset):
    """
    Action archivant les demandes sélectionnées
    """

    nb_archive = 0

    for demande in queryset.all():
        try:
            demande.archive()
            demande.save()
            nb_archive += 1
        except TransitionNotAllowed:
            modeladmin.message_user(
                request,
                "Impossible de faire passer la demande {} de l'état {} à l'état archivée".format(
                    demande.titre, demande.get_etat_display()
                ),
                level=messages.ERROR,
            )

    modeladmin.message_user(
        request,
        "{} demande{} archivée{}".format(
            nb_archive, pluralize(nb_archive), pluralize(nb_archive)
        ),
    )


archiver_demande.short_description = "Archiver les demandes sélectionnées"


@admin.register(Demande)
class DemandeAdmin(OurAdmin):
    class Media:
        css = {
            "all": ("css/global.css",),
        }

    fieldsets = [
        (
            "Infos générales : ",
            {
                "fields": [
                    "titre",
                    "type",
                    "date_debut",
                    "date_fin",
                    "lieu_deroulement",
                    "responsable",
                ]
            },
        ),
        ("Détails : ", {"fields": ["etat", "created_at", "updated_at", "created_by"]}),
        (
            "Effectifs : ",
            {
                "fields": [
                    "nb_benevoles",
                    "date_presence_debut",
                    "date_presence_fin",
                    "lieu_rdv",
                    "qualifications",
                ]
            },
        ),
        (
            "Subsistances : ",
            {"fields": ["nb_repas", "lieu_repas", "commentaires_repas"]},
        ),
        (
            "Véhicules : ",
            {
                "fields": [
                    "nb_transport",
                    "nb_log",
                    "nb_vl",
                    "nb_vpsp",
                    "commentaire_transport",
                ]
            },
        ),
        (
            "Logistique : ",
            {
                "fields": [
                    "nature_chargement",
                    "est_alimentaire",
                    "est_frais",
                    "moyens_engages",
                ]
            },
        ),
        (
            "Infos complémentaire : ",
            {"fields": ["contact_in_situ", "tel_in_situ", "description_detaillee"]},
        ),
        (
            "Demandes spécifiques : ",
            {
                "fields": [
                    "nb_PASS",
                    "nb_colis",
                    "nb_beneficiaire_h",
                    "nb_beneficiaire_f",
                    "nb_beneficiaire_enfant",
                    "eau_potable",
                    "frigo",
                    "cuisiner",
                    "risque_sanitaire",
                    "nb_colis_type_1",
                    "nb_colis_type_2",
                    "nb_colis_type_3",
                    "nb_beneficiaire",
                    "typologie_beneficiaire",
                ]
            },
        ),
    ]
    list_display = (
        "date_debut",
        "colored_etat",
        "titre",
        "type",
        "responsable",
    )
    ordering = ("-date_debut",)
    search_fields = ("titre",)
    list_filter = (
        "type",
        "etat",
    )
    readonly_fields = (
        "created_at",
        "updated_at",
        "created_by",
    )
    actions = [archiver_demande]


@admin.register(Reponse)
class ReponseAdmin(OurAdmin):
    list_display = (
        "demande",
        "commentaires",
    )
    search_fields = ("demande__titre",)


@admin.register(Retour)
class RetourAdmin(OurAdmin):
    list_display = (
        "demande",
        "accident",
        "commentaire",
    )


# Effectif
@admin.register(Effectif1J)
class Effectif1JAdmin(OurAdmin):
    list_display = (
        "nom",
        "prenom",
        "email",
        "telephone",
    )
    search_fields = (
        "nom",
        "prenom",
        "email",
    )


@admin.register(EffectifCRF)
class EffectifCRFAdmin(OurAdmin):
    list_display = (
        "nom",
        "prenom",
        "email",
        "telephone",
        "NIVOL",
    )
    search_fields = (
        "nom",
        "prenom",
        "email",
        "NIVOL",
    )


@admin.register(Competence)
class CompetenceAdmin(OurAdmin):
    list_display = (
        "denomination",
        "competenceType",
    )
    list_filter = ("competenceType",)


# Véhicule
@admin.register(Vehicule)
class VehiculeAdmin(OurAdmin):
    list_display = (
        "denomination",
        "typeVehicule",
        "nbPlacesNormales",
        "nbPlacesCovid",
        "qteDispo",
        "qteTotal",
        "get_lieu",
    )
    list_filter = (
        "typeVehicule",
        "lieu__label",
    )
    search_fields = ("denomination",)

    def get_lieu(self, obj):
        return obj.lieu.__str__()

    get_lieu.short_description = "Lieu"


# Matériel
@admin.register(Materiel)
class MaterielAdmin(OurAdmin):
    list_display = (
        "denomination",
        "typeMateriel",
        "qteDispo",
        "qteTotal",
        "get_lieu",
    )
    list_filter = (
        "typeMateriel",
        "lieu__label",
    )
    search_fields = ("denomination",)

    def get_lieu(self, obj):
        return obj.lieu.__str__()

    get_lieu.short_description = "Lieu"


# Logistique
@admin.register(Reservation)
class ReservationAdmin(OurAdmin):
    list_display = (
        "reponse",
        "get_ressource",
        "quantite",
    )

    def get_ressource(self, obj):
        if hasattr(obj.ressource, "materiel"):
            return obj.ressource.materiel
        if hasattr(obj.ressource, "vehicule"):
            return obj.ressource.vehicule
        if hasattr(obj.ressource, "effectif1j"):
            return obj.ressource.effectif1j
        if hasattr(obj.ressource, "effectifcrf"):
            return obj.ressource.effectifcrf

    get_ressource.short_description = "Ressource"


@admin.register(Disponibilite)
class DisponibiliteAdmin(OurAdmin):
    list_display = (
        "debut",
        "fin",
    )


admin.site.register(TypeVehicule)
admin.site.register(CategorieVehicule)
admin.site.register(TypeMateriel)
admin.site.register(CategorieMateriel)
admin.site.register(LieuStockage)


@admin.register(Permission)
class PermissionAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "codename",
    )

from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, Row, Column
from django import forms

from CoronApp.models.retour import *


class RetourLogistiqueForm(forms.ModelForm):
    class Meta:
        model = RetourLogistique
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourLogistiqueForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column("dons_alim", css_class="form-group col-md-6 mb-0"),
                    Column("dons_hygiene", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("dons_EPI", css_class="form-group col-md-6 mb-0"),
                    Column("dons_autres", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
            )
        )


class RetourMaraudeForm(forms.ModelForm):
    class Meta:
        model = RetourMaraude
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourMaraudeForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset("Retour spécifique", "nb_beneficiaire_rencontres")
        )


class RetourAideAlimForm(forms.ModelForm):
    class Meta:
        model = RetourAideAlim
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourAideAlimForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(Fieldset("Retour spécifique", "nb_colis_distrib"))


class RetourColisUrgenceForm(forms.ModelForm):
    class Meta:
        model = RetourColisUrgence
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourColisUrgenceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column("nb_beneficiaire", css_class="form-group col-md-4 mb-0"),
                    Column("nb_colis", css_class="form-group col-md-4 mb-0"),
                    Column(
                        "nb_beneficiaires_inscrits",
                        css_class="form-group col-md-4 mb-0",
                    ),
                    css_class="form-row",
                ),
            )
        )


class RetourCrChezVousForm(forms.ModelForm):
    class Meta:
        model = RetourCrChezVous
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourCrChezVousForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column(
                        "nb_livraisons_effectuees", css_class="form-group col-md-4 mb-0"
                    ),
                    Column(
                        "nb_livraisons_echouees", css_class="form-group col-md-4 mb-0"
                    ),
                    Column("nb_paniers", css_class="form-group col-md-4 mb-0"),
                    css_class="form-row",
                ),
                "signalements_transmis",
            )
        )


class RetourMissionUrgenceForm(forms.ModelForm):
    class Meta:
        model = RetourMissionUrgence
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourMissionUrgenceForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column("nb_lots_PAI", css_class="form-group col-md-6 mb-0"),
                    Column("nb_lots_CAI", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("nb_lots_CHU", css_class="form-group col-md-6 mb-0"),
                    Column("nb_lots_CMCC", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                "nb_beneficiaire",
            )
        )


class RetourAccueilJourForm(forms.ModelForm):
    class Meta:
        model = RetourAccueilJour
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourAccueilJourForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(Fieldset("Retour spécifique", "nb_beneficiaire"))


class RetourSecoursPublicsForm(forms.ModelForm):
    class Meta:
        model = RetourSecoursPublics
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourSecoursPublicsForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column("nb_victimes_vues", css_class="form-group col-md-6 mb-0"),
                    Column("nb_UA", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column(
                        "nb_interventions_eventuelles",
                        css_class="form-group col-md-4 mb-0",
                    ),
                    Column("nb_evac_CRF", css_class="form-group col-md-4 mb-0"),
                    Column("nb_evac_non_CRF", css_class="form-group col-md-4 mb-0"),
                    css_class="form-row",
                ),
            )
        )


class RetourDesserrementForm(forms.ModelForm):
    class Meta:
        model = RetourDesserrement
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourDesserrementForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column("nb_beneficiaire", css_class="form-group col-md-6 mb-0"),
                    Column("nb_VPSP_non_CRF", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
                Row(
                    Column("nb_VPSP_CRF", css_class="form-group col-md-6 mb-0"),
                    Column("nb_hors_VPSP", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
            )
        )


class RetourDistributionForm(forms.ModelForm):
    class Meta:
        model = RetourDistribution
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourDistributionForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout(
            Fieldset(
                "Retour spécifique",
                Row(
                    Column("nb_beneficiaire", css_class="form-group col-md-6 mb-0"),
                    Column("nb_colis", css_class="form-group col-md-6 mb-0"),
                    css_class="form-row",
                ),
            )
        )


class RetourContinuiteForm(forms.ModelForm):
    class Meta:
        model = RetourContinuite
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourContinuiteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout()


class RetourAccueilEcouteForm(forms.ModelForm):
    class Meta:
        model = RetourAccueilEcoute
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourAccueilEcouteForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout()


class RetourSoutienOpForm(forms.ModelForm):
    class Meta:
        model = RetourSoutienOp
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourSoutienOpForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout()


class RetourNonDefiniForm(forms.ModelForm):
    class Meta:
        model = RetourNonDefini
        exclude = ["retour"]

    def __init__(self, *args, **kwargs):
        super(RetourNonDefiniForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False

        self.helper.layout = Layout()

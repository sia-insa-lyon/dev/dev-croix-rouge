from django.db import models


class Competence(models.Model):
    # Dénomination, ex GQS, TCAU, PSC1...
    denomination = models.CharField(
        verbose_name="Acronyme de la compétence", help_text="Ex: PSC1", max_length=128
    )

    description = models.TextField(
        verbose_name="Signification de l'acronyme",
        help_text="Ex: Premiers Secours en Equipe de niveau 1",
    )
    permissionBNVCRF = models.BooleanField(
        verbose_name="Permission BNV CRF",
        help_text="Est-ce que les bénévoles CRF  peuvent avoir cette compétence ?",
        default=True,
    )
    permissionBNV1J = models.BooleanField(
        verbose_name="Permission BNV 1J",
        help_text="Est-ce que les bénévoles 1J  peuvent avoir cette compétence ?",
    )
    competenceParente = models.ForeignKey(
        "CoronApp.Competence",
        verbose_name="compétence parente",
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
    )
    competenceType = models.ForeignKey(
        "CoronApp.TypeCompetence",
        verbose_name="type de la compétence",
        on_delete=models.SET_NULL,
        null=True,
    )

    def __str__(self):
        return "Competence " + self.denomination


class TypeCompetence(models.Model):
    """
    Exemples : SECOURS, SOCIAL etc.
    """

    label = models.CharField(
        verbose_name="nom du type de la competence", max_length=128, unique=True
    )

    def __str__(self):
        return self.label

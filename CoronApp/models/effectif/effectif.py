from django.db import models
from django.utils.datetime_safe import datetime
from phonenumber_field.modelfields import PhoneNumberField

from CoronApp.models import Ressource


# ABSTRACT
from CoronApp.models.ressource import RessourcePointer


class Effectif(Ressource):
    class Meta:
        abstract = True

    nom = models.CharField(verbose_name="nom", max_length=128)
    prenom = models.CharField(verbose_name="prenom", max_length=128)

    email = models.EmailField(
        verbose_name="email", editable=False
    )  # Identification par email, donc pas blank
    telephone = models.CharField(verbose_name="numéro de téléphone", max_length=20)
    commentaire = models.TextField(verbose_name="commentaire", blank=True)

    disponibilites = models.ManyToManyField(
        "CoronApp.Disponibilite", verbose_name="disponibilités", blank=True
    )
    competences = models.ManyToManyField(
        "CoronApp.Competence", verbose_name="compétences", blank=True
    )

    def __str__(self):
        return "Effectif " + self.nom + " " + self.prenom

    def est_dispo(self, date_debut=None, date_fin=None, demande=None, quantite=1):
        if demande:
            date_debut = demande.date_presence_debut
            date_fin = demande.date_presence_fin

        # Les disponibilités de l'effectif
        dispos = self.disponibilites

        for dispo in dispos.all():
            # Si le créneau est inclus dans la disponibilitée
            if (
                dispo.debut <= date_debut < dispo.fin
                and dispo.debut < date_fin <= dispo.fin
            ):
                # On vérifie qu'il n'y a pas de problèmes avec d'autres réservations
                return Ressource.est_dispo(
                    self, date_debut=date_debut, date_fin=date_fin
                )

        # Si aucune disponibilité ne s'intercale dans le créneau demandé, la ressource n'est pas dispo
        return False


class EffectifCRF(Effectif):
    """Bénévole Croix-Rouge"""

    ressource = models.OneToOneField(
        RessourcePointer, on_delete=models.CASCADE, default=None, blank=True
    )
    NIVOL = models.CharField(verbose_name="NIVOL", max_length=128, unique=True)

    def __str__(self):
        return "EffectifCRF : " + self.nom + " " + self.prenom


class Effectif1J(Effectif):
    """Bénévole d'un jour"""

    ressource = models.OneToOneField(
        RessourcePointer, on_delete=models.CASCADE, default=None, blank=True
    )
    type = models.CharField(verbose_name="type", max_length=120, blank=True, null=True)

    def __str__(self):
        return "Effectif1J : " + self.nom + " " + self.prenom


class IntersectionDisponibiliteError(ValueError):
    def __str__(self):
        return "Erreur: Les disponibilitées s'intersectent (on ne peut pas être à la fois au four et au moulin !)"

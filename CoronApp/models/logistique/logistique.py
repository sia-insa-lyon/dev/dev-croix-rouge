from django.db import models
from CoronApp.models import Ressource


# ABSTRACT
class Logistique(Ressource):
    """Parent de matériel and véhicule"""

    class Meta:
        abstract = True

    lieu = models.ForeignKey(
        "CoronApp.LieuStockage",
        on_delete=models.SET_NULL,
        verbose_name="lieu",
        null=True,
    )

    denomination = models.CharField(
        verbose_name="Dénomination de la ressource", max_length=128
    )
    remarque = models.TextField(verbose_name="remarque", blank=True, null=True)
    qteTotal = models.PositiveIntegerField(verbose_name="quantité totale", default=1)
    qteDispo = models.PositiveIntegerField(
        verbose_name="quantité disponible", default=1
    )

    def __str__(self):
        return self.denomination

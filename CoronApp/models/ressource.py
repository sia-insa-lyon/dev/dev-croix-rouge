# coding=utf-8
from django.db import models

# ABSTRACT
from CoronApp.models import Demande


class Ressource(models.Model):
    class Meta:
        abstract = True

    """
    Retourne vrai si la ressource est allouée à la réponse passée en paramètre
    """

    def est_alloue_reponse(self, reponse):
        # On regarde si la ressource est déja allouée à cette réponse
        for resa in self.ressource.reservation_set.all():
            if resa.reponse == reponse:
                return True
        return False

    """
        Retourne vrai si la ressource est allouée à la réponse passée en paramètre
        """

    def get_reservation_commune(self, reponse):
        # On regarde si la ressource est déja allouée à cette réponse
        for resa in self.ressource.reservation_set.all():
            if resa.reponse == reponse:
                return resa
        return None

    """ Retourne vrai si la ressource est disponible pendant le créneau donné
    /!\ ne prend pas en compte la quantité pour le moment
    
    Attention pour le test, mettre le datetime en UTC pour ne pas avoir d'erreur: datetime([...], tz=timezone.utc),
    décalé de 2h en retard sur l'heure serveur
    Ex: vehicule.estDispo(datetime(2020,4,21,14,25,tzinfo=timezone.utc),(datetime(2020,4,21,14,55,tzinfo=timezone.utc)))
    """

    def est_dispo(self, date_debut=None, date_fin=None, demande=None, quantite=1):
        if demande != None:
            date_debut = demande.date_debut
            date_fin = demande.date_fin

        # Toutes les missions où l'objet est réservé
        demandes = Demande.objects.filter(
            reponse__reservations__ressource=self.ressource
        )
        quantite_dispo = self.qteDispo if hasattr(self, "qteDispo") else 1

        # Solution provisoire avant matériel
        if quantite_dispo == 0:
            return False

        # TODO : Vérifier l'état de la demande ?
        for demande in demandes:
            if (
                demande.date_debut <= date_debut < demande.date_fin
                or demande.date_debut < date_fin <= demande.date_fin
                or date_debut <= demande.date_debut < date_fin
            ):
                return False

        # Si aucune réservation ne s'intercale dans le créneau demandé, la ressource est dispo
        return True

    def save(self, *args, **kwargs):
        if not hasattr(self, "ressource"):
            self.ressource = RessourcePointer()
            self.ressource.save()
        super(Ressource, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        if hasattr(self, "ressource"):
            self.ressource.delete()
        super(Ressource, self).delete(*args, **kwargs)


"""
Solution 3 proposée ici:
https://lukeplant.me.uk/blog/posts/avoid-django-genericforeignkey/

Pour ne pas avoir une FK qui pointe vers une ressource abstraite, on crée une classe intermédiaire
en relation OneToOne avec une implémentation de la ressource pour aller chercher la ressource depuis la réservation
(l'inverse est possible aussi avec Materiel.ressource.reservation_set.all())

Il faut donc créer un ressourcePointer pour chaque ressource créee :
ressource = models.OneToOneField(RessourcePointer, on_delete=models.CASCADE, default=None, blank=True)
dans chaque implémentation de ressource

Pour connaitre le type de ressource associée à la réservation, on peut uttiliser hasattr(materiel.ressource, 'vehicule')
"""


class RessourcePointer(models.Model):
    def __str__(self):
        if hasattr(self, "materiel"):
            return self.materiel.__str__()
        if hasattr(self, "vehicule"):
            return self.vehicule.__str__()
        if hasattr(self, "effectif1j"):
            return self.effectif1j.__str__()
        if hasattr(self, "effectifcrf"):
            return self.effectifcrf.__str__()
        return "inconnu"

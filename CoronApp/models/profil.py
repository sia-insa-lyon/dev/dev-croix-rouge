# coding=utf-8

from django.db import models
from django.contrib.auth.models import User


# Create your models here.


class Profil(models.Model):
    class Meta:
        permissions = [
            ("is_chef", "User is chef"),
            ("peut_importer", "Peut importer des documents"),
        ]

    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name="profil")
    tel = models.CharField(max_length=20)

    def __str__(self):
        return self.user.get_full_name()

class DataToImport:
    """ Classe générique d'import dont descendent toutes les autres classes de traitement d'import """

    df_object = None  # page
    key_col_name = None  # nom de la colonne de l'excel contenant la pk
    key_obj_name = None  # attribut pk django
    obj = None  # objet à créer

    def __init__(self, df_object, key_col_name, key_obj_name, obj):
        self.df_object = df_object
        self.key_col_name = key_col_name
        self.key_obj_name = key_obj_name
        self.obj = obj

    def process_data(self, row, key):
        """ Implémentée dans la classe fille """
        pass


class ListToImport(DataToImport):
    def process_data(self, row, key):
        data_to_save = self.obj(
            label=ImportDataUtility.process_data(row, self.key_col_name),
        )
        data_to_save.save()


class ImportDataUtility:
    COMP_SEPARATOR = "/"
    UNSUPPORTED_CHAR = [" ", "-"]
    ROW_NUMBER_OFFSET = 2

    @staticmethod
    def process_data(row, col_name):
        """Process les données bruts : NaN --> None et les float en int"""

        data = getattr(row, col_name)
        if data == data:  # NaN != NaN
            if isinstance(data, float):
                return int(data)
            else:
                return data
        else:
            return None

    @staticmethod
    def process_data_object(row, col_name, obj):
        """
        Cherche la foreign key associée à l'objet obj,
        par exemple Vehicule --> LieuStockage (obj sera LieuStockage)
        """

        data = ImportDataUtility.process_data(row, col_name)
        if data is not None:
            return obj.objects.get(label=getattr(row, col_name))
        else:
            raise Exception(
                "a tenté de créer une clef étrangère à partir d'un objet inexistant ("
                + obj.__name__
                + ")"
            )

    @staticmethod
    def process_data_object_or_None(row, col_name, obj):
        """
        Cherche la foreign key associée à l'objet obj,
        par exemple Vehicule --> LieuStockage (obj sera LieuStockage)
        """

        data = ImportDataUtility.process_data(row, col_name)
        if data is not None:
            return obj.objects.get(label=getattr(row, col_name))
        else:
            return None

    @staticmethod
    def format_for_list(obj, name=None, row=None, exception=None):
        res = ""
        if row is not None:
            res += "Ligne " + str(row + ImportDataUtility.ROW_NUMBER_OFFSET) + ": "

        res += obj.__name__ + " "

        if name is not None:
            res += name + " "
        if exception is not None:
            res += str(exception)

        return res


class ImportData:
    add_list = []
    error_list = []
    untouched_list = []

    def __init__(self):
        self.add_list = []
        self.error_list = []
        self.untouched_list = []

    def process_list(self, object_to_import: DataToImport):
        """Process une liste, comme par exemple les catégories de véhicule
            Une liste est une colonne dans un objet, ne contient qu'un label"""

        list_of_objects = list(
            set(object_to_import.df_object[object_to_import.key_col_name])
        )
        for one_obj in list_of_objects:
            if one_obj == one_obj:  # Vérifie que la valeur n'est pas NaN (cellule vide)
                if not object_to_import.obj.objects.filter(label=one_obj).exists():
                    try:
                        object_to_import.obj(label=one_obj).save()
                        self.add_list.append(
                            ImportDataUtility.format_for_list(
                                object_to_import.obj, one_obj
                            )
                        )
                    except Exception as exception:
                        self.error_list.append(
                            ImportDataUtility.format_for_list(
                                object_to_import.obj, one_obj, None, exception
                            )
                        )
                else:
                    self.untouched_list.append(
                        ImportDataUtility.format_for_list(object_to_import.obj, one_obj)
                    )

    def process_object(self, object_to_import: DataToImport):
        """ Process un objet complexe, comme par exemple Vehicule, Effectif """

        for line_number, row in enumerate(
            object_to_import.df_object.itertuples(index=True, name="Pandas")
        ):
            try:
                key_value = ImportDataUtility.process_data(
                    row, object_to_import.key_col_name
                )  # Clef primaire
                if not object_to_import.obj.objects.filter(
                    **{object_to_import.key_obj_name: key_value}
                ).exists():  # Vérification qu'il n'existe pas encore
                    try:
                        # Fonction qui traite les données (matching col excel - objet django)
                        object_to_import.process_data(row, key_value)
                        self.add_list.append(
                            ImportDataUtility.format_for_list(
                                object_to_import.obj, key_value, line_number
                            )
                        )
                    except Exception as exception:
                        self.error_list.append(
                            ImportDataUtility.format_for_list(
                                object_to_import.obj, key_value, line_number, exception
                            )
                        )
                else:
                    self.untouched_list.append(
                        ImportDataUtility.format_for_list(
                            object_to_import.obj, key_value, line_number
                        )
                    )
            except Exception as exception:
                self.error_list.append(
                    ImportDataUtility.format_for_list(
                        object_to_import.obj, None, line_number, exception
                    )
                )

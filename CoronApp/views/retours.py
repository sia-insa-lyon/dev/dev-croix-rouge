from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render, redirect
from django.views.generic import UpdateView

from CoronApp.forms import *
from CoronApp.models import Retour, Demande
from CoronApp.models.retour import *


class RetourView(LoginRequiredMixin, UpdateView):
    login_url = "/accounts/login"
    model = Retour
    form_class = RetourForm
    template_name = "demande/form-retour.html"

    def get(self, request, *args, **kwargs):

        demande = get_object_or_404(Demande, pk=self.kwargs["pk"])

        # Seul le responsable de la mission édite le retour, et les chefs
        if (
            request.user.id != demande.responsable.user.id
            and not request.user.has_perm("CoronApp.is_chef")
        ):
            raise PermissionDenied()  # Sinon c'est dead

        retour_form = RetourForm(instance=demande.retour)
        retour_form_spe = self.get_retour_spe_form_class(demande)(
            instance=demande.retour.get_retour_spe()
        )

        return render(
            request,
            template_name="demande/form-retour.html",
            context={
                "retourForm": retour_form,
                "retourFormSpe": retour_form_spe,
                "id_dde": demande.pk,
            },  # Pour compléter l'URL de soumission du form
        )

    def post(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, pk=self.kwargs["pk"])

        if (
            request.user.id != demande.responsable.user.id
            and not request.user.has_perm("CoronApp.is_chef")
        ):
            raise PermissionDenied()  # Forbidden

        # get_retour_spe_form_class retourne la classe du form spécifique, on l'instancie
        # Bizarre mais c'est Python
        retour_spe_form = self.get_retour_spe_form_class(demande)(
            request.POST, instance=demande.retour.get_retour_spe()
        )
        retour_form = RetourForm(request.POST, instance=demande.retour)

        if not (retour_form.is_valid() and retour_spe_form.is_valid()):
            return HttpResponse(status=400)

        retour_spe_form.save()
        retour_form.save()

        # Pour passer de clôturé à terminé si ce n'est pas déjà fait
        if demande.etat != "termine":
            demande.termine()
            demande.save()

        return redirect(reverse("demande", kwargs={"pk": demande.pk}))

    def get_success_url(self):
        retour = Retour.objects.get(pk=self.kwargs["pk"])
        return reverse("demande", kwargs={"pk": retour.demande.pk})

    @staticmethod
    def get_retour_spe_form_class(demande):
        """ La classe de form ne peut pas être retournée depuis le modèle Retour (import circulaire),
        qui ne fournit que le nom de la classe, cette fonction va retourner la classe en elle-même
        """

        return getattr(sys.modules[__name__], demande.retour.get_spe_form_class_name())

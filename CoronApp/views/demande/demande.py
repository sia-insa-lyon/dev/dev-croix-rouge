from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.views.generic import CreateView, UpdateView, ListView, DetailView

from CoronApp.forms import DemandeForm
from CoronApp.models import Demande
from CoronApp.models.logistique.vehicule import Vehicule
from CoronApp.views.assignations.assignations import peut_assigner


class DemandeFormView(LoginRequiredMixin, CreateView):
    login_url = "/accounts/login"
    model = Demande
    form_class = DemandeForm
    template_name = "demande/form-demande.html"
    success_url = "/demandes/"

    def get_initial(self):
        if hasattr(self.request.user, "profil"):
            return {"responsable": self.request.user.profil}
        return None

    def get(self, request, *args, **kwargs):
        if not self.request.user.has_perm("CoronApp.add_demande"):
            raise PermissionDenied()
        return super(DemandeFormView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        if not self.request.user.has_perm("CoronApp.add_demande"):
            raise PermissionDenied()

        demande = form.save(commit=False)
        demande.created_by = self.request.user
        demande.save()

        return super(DemandeFormView, self).form_valid(form)


class ListDemandes(LoginRequiredMixin, ListView):
    login_url = "/accounts/login"
    model = Demande
    paginate_by = 100
    template_name = "demande/list-demandes.html"
    queryset = Demande.objects.exclude(etat="archive")
    context_object_name = "demandes"


class DemandeEditView(LoginRequiredMixin, UpdateView):
    login_url = "/accounts/login"
    model = Demande
    form_class = DemandeForm
    template_name = "demande/form-edit-demande.html"
    success_url = "/demandes/"

    def get(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, pk=self.kwargs["pk"])

        if not (
            (
                demande.created_by
                and request.user.id == demande.created_by.id
                and demande.etat == "cree"
            )
            or (
                request.user.has_perm("CoronApp.can_edit")
                and demande.etat not in ["cloture", "termine"]
            )
        ):
            raise PermissionDenied()

        form = DemandeForm(instance=demande)
        form.helper.form_action = reverse(
            "demande-edit", kwargs={"pk": self.kwargs["pk"]}
        )

        # Désactive l'édition des champs date
        # Disabled bloque l'envoi, donc on utilise readlony
        if demande.etat != "cree":
            form.fields["date_fin"].widget.attrs["readonly"] = True
            form.fields["date_debut"].widget.attrs["readonly"] = True
            form.fields["date_presence_debut"].widget.attrs["readonly"] = True
            form.fields["date_presence_fin"].widget.attrs["readonly"] = True

        return render(request, self.template_name, {"form": form, "demande": demande})

    def post(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, pk=self.kwargs["pk"])

        if (request.user.id == demande.created_by.id and demande.etat == "cree") or (
            request.user.has_perm("CoronApp.can_edit")
            and demande.etat not in ["cloture", "termine"]
        ):
            return super(DemandeEditView, self).post(request, *args, **kwargs)
        raise PermissionDenied()

    def get_success_url(self):
        demande = Demande.objects.get(pk=self.kwargs["pk"])
        return reverse("demande", kwargs={"pk": demande.pk})


# Rempli le contexte selon les droits de l'utilisateur concernant les assignations
def ajout_des_assignations(request, reponse, context):
    for type in ["effectifs", "vehicules", "logistique", "subsistances"]:
        if peut_assigner(request.user, reponse, type):
            context["peut_assigner_" + type] = True
    return context


class DemandeView(LoginRequiredMixin, DetailView):
    login_url = "/accounts/login"
    model = Demande
    template_name = "demande/detail-demande.html"
    context_object_name = "demande"

    def get(self, request, *args, **kwargs):
        demande = get_object_or_404(Demande, pk=self.kwargs["pk"])

        context = {"demande": demande}
        # Affichage des ressources
        if hasattr(demande, "reponse"):
            context["vehicules"] = Vehicule.objects.filter(
                ressource__reservation__in=demande.reponse.reservations.all()
            )

            # Ajout des permissions sur les assignations
            context = ajout_des_assignations(request, demande.reponse, context)

        if hasattr(demande, "retour"):
            context["retour_spe"] = demande.retour.get_retour_spe()
        print(context)
        return render(request, self.template_name, context)
